set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
" To install Vundle run:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
"
" Nice exmaple vimrc gile
" https://github.com/filipesf/dotfiles/blob/master/vimrc

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Just a shitload of color schemes.
" https://github.com/flazz/vim-colorschemes#current-colorschemes
" Bundle 'flazz/vim-colorschemes'
Plugin 'dracula/vim'

Plugin 'vim/killersheep'

Plugin 'altercation/vim-colors-solarized'

" Support for easily toggling comments.
" Hit 'gcc' to comment out a line
Bundle 'tpope/vim-commentary'

" Proper JSON filetype detection, and support.
Bundle 'leshill/vim-json'

" Good syntax highlighting for markdown
Bundle 'tpope/vim-markdown'

Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

Plugin 'edkolev/tmuxline.vim'

" Python docs
Plugin 'davidhalter/jedi-vim'

" Indent lines for python
Plugin 'Yggdroot/indentLine'

" Git plugin for vim
Plugin 'tpope/vim-fugitive'

" Syntax checker for VIM. Very complex
Plugin 'vim-syntastic/syntastic'

" Autocomplete for multiple languages
" Plugin 'Valloric/YouCompleteMe'
" Plugin 'sean-mcmahon/YouCompleteMe'
" Plugin 'neoclide/coc.nvim', {'branch': 'release'}

" Overview of file
" Depends on universal-ctags - https://github.com/universal-ctags/ctags/blob/master/docs/autotools.rst#gnu-linux-distributions
Plugin 'majutsushi/tagbar'

" Better Syntax highlighting
Plugin 'sheerun/vim-polyglot'

Plugin 'tpope/vim-vinegar'

" Add ds, cs, and yss
Plugin 'tpope/vim-surround'

" Puts vim into paste mode when using ctrl+shift+v into terminal (should work
" for mac too)
Plugin 'ConradIrwin/vim-bracketed-paste'

" Vim and tmux integration
Plugin 'benmills/vimux'

Plugin 'tpope/vim-repeat'

Plugin 'JamshedVesuna/vim-markdown-preview'

Plugin 'pboettch/vim-cmake-syntax'

Plugin 'tell-k/vim-autopep8'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Setting this keeps the Vim theme working in Tmux
" From https://github.com/tmux/tmux/issues/699

" Auto close brackets
source /home/sean/.vim/autoclose.vim
" set ttimeout

set t_Co=256

let &t_TI = ""
let &t_TE = ""

syntax on " Syntax highlighting
set nu
set smartindent " Intellegently dedent / indent new lines based on rules.
set hlsearch
set cursorline
set ignorecase " ignore case
set smartcase  " match case if any uppercase used
set wildmenu            " visual autocomplete for command menu
set autoread " If file changes outside of Vim, reload
set relativenumber
set formatoptions+=j " Delete comment character when joining commented lines
" Toggle Relative Number
nnoremap <silent> <leader>nb :set relativenumber!<CR>

" filetype plugin on
" set omnifunc=syntaxcomplete#Complete
let g:solarized_italic=0
let g:dracula_italic = 0
let g:dracula_colorterm = 0
call togglebg#map("<F2>")

if $VIM_COLOR == 'dracula'
	color dracula
	" colorscheme dracula
elseif $VIM_COLOR == 'solar_light'
	" let g:solarized_termcolors=256
	colorscheme solarized
else
  " Default is dracula
  color dracula
endif

" for vim-markdown-preview to work with grip
let vim_markdown_preview_github=1

let g:tmuxline_powerline_separators = 0
let airline#extensions#tmuxline#color_template = 'insert'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#tabline#tab_nr_type = 1 " tab number

" No time in tmuxline
" Status info depends on https://github.com/thewtex/tmux-mem-cpu-load
let g:tmuxline_preset = {
      \'a'    : '#S',
      \'win'  : ['#I', '#W'],
      \'cwin' : ['#I', '#W'],
      \'x'    : ['#(tmux-mem-cpu-load -i 1 -a 0 -g 10)'],
      \'y'    : ['%Y-%m-%d'],
      \'z'    : '#h',
      \'options' : {'status-justify' : 'left'}}
      " \'y'    : ['%Y-%m-%d','%H:%M'],
      " \'y'    : ['%Y-%m-%d','%H'],
      " \'y'    : ['%Y-%m-%d'],
      " \'x'    : ['#(tmux-mem-cpu-load -i 2 -a 0 -g 10)'],

" Close Tabgar when switching tabs, Tabgar seems to crash after a while, tring
" to fix this
let g:airline#extensions#tabline#buffer_idx_mode = 1
" nmap <leader>1 :TagbarClose<CR><Plug>AirlineSelectTab1
" nmap <leader>2 :TagbarClose<CR><Plug>AirlineSelectTab2
" nmap <leader>3 :TagbarClose<CR><Plug>AirlineSelectTab3
" nmap <leader>4 :TagbarClose<CR><Plug>AirlineSelectTab4
" nmap <leader>5 :TagbarClose<CR><Plug>AirlineSelectTab5
" nmap <leader>6 :TagbarClose<CR><Plug>AirlineSelectTab6
" nmap <leader>7 :TagbarClose<CR><Plug>AirlineSelectTab7
" nmap <leader>8 :TagbarClose<CR><Plug>AirlineSelectTab8
" nmap <leader>9 :TagbarClose<CR><Plug>AirlineSelectTab9
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
" one 'n' does not work well as n in next
" nmap <leader>nn :TagbarClose<CR><Plug>AirlineSelectNextTab
" nmap <leader>p :TagbarClose<CR><Plug>AirlineSelectPrevTab
let g:tagbar_ctags_bin = '/snap/bin/ctags'
let g:tagbar_use_cache = 0
nmap <leader>nn <Plug>AirlineSelectNextTab
nmap <leader>p <Plug>AirlineSelectPrevTab
" tagbar shortcut
nmap <leader>t :TagbarToggle<CR>
" Show line nubers in Tagbar. Possible values are:
"   0: Don't show any line numbers.
"   1: Show absolute line numbers.
"   2: Show relative line numbers.
"   -1: Use the global line number settings.
let g:tagbar_show_linenumbers = -1
" let g:tagbar_left = 1
let g:tagbar_position = 'left'

" let g:netrw_winsize = 12
let g:netrw_browse_split=0
" Netrw with line numbers, from https://stackoverflow.com/questions/8730702/how-do-i-configure-vimrc-so-that-line-numbers-display-in-netrw-in-vim
let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro'

" let g:airline#extensions#tabline#buffer_nr_show = 1

let g:markdown_syntax_conceal = 0
" No docstring window popup from Jedi
autocmd FileType python setlocal completeopt-=preview
" let g:jedi#use_splits_not_buffers='winwidth'

" YCM config
" Disable popup window
nmap <leader>s <plug>(YCMHover)
let g:ycm_auto_hover=''

" Somehow add:
" hi TabLineFill ctermfg=Black ctermbg=DarkBlue
" to make tabline dark

" Read .launch files with XML syntax
autocmd BufRead,BufNewFile *.launch set filetype=xml


" Recommended Syntastic settings to start with
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 2
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" For only syntax error checks for python (3).
let g:syntastic_python_checkers=['pyflakes']
" let g:syntastic_python_checkers=['pycodestyle']
autocmd FileType python noremap <buffer> <F7> :SyntasticCheck pycodestyle<CR>
" let g:syntastic_python_checker_args='--ignore=E231'
let g:syntastic_python_pycodestyle_post_args="--ignore=E231,E252,E701,E731 --max-line-length=100"
" Syntastic does tex conceal, turn that off.
let g:tex_conceal = ""

" Auto pep8
autocmd FileType python noremap <buffer> <F8> :call Autopep8()<CR>
let g:autopep8_aggressive=0
" let g:autopep8_disable_show_diff=0
" 701 & 70 do not put colon and semicolon separeted statements on separate
" lines
let g:autopep8_ignore="E203,E231,E252,E731,E24,W6,E701,E70"
let g:autopep8_max_line_length=100

" ------------------
" My Attempts to make :W be the same as :w
" From: https://stackoverflow.com/questions/117150/can-i-remap-ex-commands-in-vim
" Not convinced this command way is best
" command WQ wq
" command Wq wq
" command W w
" command Q q
"
" Top answers from here
" https://stackoverflow.com/questions/10590165/is-there-a-way-in-vim-to-make-w-to-do-the-same-thing-as-w
" command! -bang -range=% -complete=file -nargs=* W <line1>,<line2>write<bang> <args>
" 
" Perhaps this is better, for ":W" problem:
" From: https://stackoverflow.com/questions/3878692/how-to-create-an-alias-for-a-command-in-vim/3879737#3879737
fun! SetupCommandAlias(from, to)
  exec 'cnoreabbrev <expr> '.a:from
        \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:from.'")'
        \ .'? ("'.a:to.'") : ("'.a:from.'"))'
endfun
call SetupCommandAlias("W","w")
call SetupCommandAlias("Q","q")
call SetupCommandAlias("Wq", "wq")
call SetupCommandAlias("WQ", "wq")
call SetupCommandAlias("Qa", "qa")
" ------------------


" inoremap jj <Esc>
inoremap jk <Esc>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-=> <C-W><C-=>
nnoremap <leader>b oimport pdb; pdb.set_trace()<Esc>

" PageUp ad PageDown do not work in Insert mode
imap <PageUp> <NOP>
imap <PageDown> <NOP>

"" nnoremap <buffer> <F9> :exec '!python3' shellescape(@%, 1)<cr>
map <F5> <Esc>:wa<CR>:VimuxRunCommand("clear; python3 " . bufname("%"))<CR>
map <F4> <Esc>:wa<CR>:VimuxRunCommand("clear; python3 -m pytest " . bufname("%"))<CR>
" map <F3> <Esc>:wa<CR>:VimuxRunLastCommand<CR>
map <F3> <Esc>:wa<CR>:VimuxRunCommand("!-1")<CR>

" Colour column at
" https://superuser.com/questions/249779/how-to-setup-a-line-length-marker-in-vim-gvim
highlight ColorColumn ctermbg=Gray
call matchadd('ColorColumn', '\%101v', 100)

" Stops vim from auto indenting when adding colol ":"
" From https://stackoverflow.com/questions/19320747/prevent-vim-from-indenting-line-when-typing-a-colon-in-python
autocmd FileType python setlocal indentkeys-=<:>
autocmd FileType python setlocal indentkeys-=:


" For neoclide/coc.nvim package installed using Vims built in package manager.
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()
