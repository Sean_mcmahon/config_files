#!/bin/bash

SESSION=work
tmux -2 new -s $SESSION -d
# tmux send-keys -t $SESSION 'cd /home/sean/repos/orthoguides; source .ortho_env/bin/activate; clear' C-m
tmux send-keys -t $SESSION 'cd /home/sean/repos/bone_calcs; conda activate /home/sean/miniconda3/envs/bone_calcs_feature_extraction; clear' C-m
tmux rename-window -t $SESSION calcs

tmux new-window -t $SESSION
tmux send-keys -t $SESSION 'cd /home/sean/repos/orthoguides; conda activate /home/sean/miniconda3/envs/orthoenv; clear' C-m
tmux rename-window -t $SESSION ortho

tmux new-window -t $SESSION
tmux rename-window -t $SESSION db
tmux send-keys -t $SESSION 'cd /home/sean/repos/orthoguides_database; clear; git fetch && vi . -c Git' C-m

tmux new-window -t $SESSION
tmux rename-window -t $SESSION tests
tmux send-keys -t $SESSION 'cd /home/sean/repos/bone_calcs; conda activate /home/sean/miniconda3/envs/bone_calcs_feature_extraction; clear' C-m

tmux select-window -t $SESSION:0
tmux attach -t $SESSION
