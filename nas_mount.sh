#!/usr/bin/env bash

pswd='preciseNAS@33'

sudo mount -t cifs //192.168.5.240/1_Datasets /mnt/share -o user=sean,password=${pswd},vers=2.0

sudo mount -t cifs //192.168.5.240/4_Documents /mnt/nas_docs/ -o user=sean,password=${pswd},uid=$(id -u),gid=$(id -g),forceuid,forcegid,vers=2.0

