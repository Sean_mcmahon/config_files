#!/bin/bash

SESSION=tbs-tmux
tmux -2 new -s $SESSION -d
tmux rename-window -t $SESSION vim
tmux send-keys -t $SESSION 'cd ~/; vim .' C-m

tmux set-option -g prefix C-n

tmux new-window -t $SESSION

tmux select-window -t $SESSION:0
tmux attach -t $SESSION
